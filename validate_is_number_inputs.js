function validateIsNumberInputs(a, b) {
  if (Number.isNaN(Number(a)) || Number.isNaN(Number(b))) {
    throw new Error('invalid input');
  }
  if (typeof a === 'string' || typeof b === 'string') {
    throw new Error('input need to be number');
  }
}

module.exports = validateIsNumberInputs;
